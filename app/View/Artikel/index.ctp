<h1 align="center">Data Artikel</h1>
 <?php echo $this->Html->link('Tambah Artikel',array('action'=>'tambah')) ?>
<table>
  <thead>
    <tr>
      <th>ID</th>
      <th>Judul</th>
      <th>Konten</th>
      <th>Tanggal Dibuat</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($artikel as $a): ?>
      <tr>
        <td><?php echo $a['Artikel']['id'] ?></td>
        <td><?php echo $a['Artikel']['judul'] ?></td>
        <td><?php echo $a['Artikel']['konten'] ?></td>
        <td><?php echo $a['Artikel']['created'] ?></td>
        <td>
        	<?php echo $this->Html->link('lihat', array('action' => 'lihat', $a['Artikel']['id'])) ?>
			<?php echo $this->Html->link('Ubah', array('action' => 'ubah', $a['Artikel']['id'])) ?>
			<?php echo $this->Html->link('Hapus', array('action' => 'hapus', $a['Artikel']['id']), array('confirm' => 'Anda yakin?')) ?>
		</td>
      </tr>
    <?php endforeach ?>
  </tbody>
</table>