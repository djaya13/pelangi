<?php
 
class ArtikelController extends AppController
{
 
	public $uses = array('Artikel');
	 
	function index()
	  {
	    $artikel = $this->Artikel->find('all');
	    $this->set('artikel', $artikel);
	  }

	function tambah()
	{
	  //jika form sudah disubmit
	  if ($this->request->is('post') || $this->request->is('put'))
	  {
	    //simpan data baru dengan isi dari form
	    if ($this->Artikel->save($this->request->data))
	    {
	      //beri keterangan "Data telah disimpan."
	      $this->Session->setFlash('Data telah disimpan.');
	      //alihkan ke function index()
	      $this->redirect(array('action' => 'index'));
	    }
	  }
	}

	function ubah($id)
	{
	  $this->Artikel->id = $id;
	 
	  //tambahkan kode sama seperti kode yang ada di dalam tambah()
	  if ($this->request->is('post') || $this->request->is('put') )
	  {
	    if ($this->Artikel->save($this->request->data))
	    {
	      $this->Session->setFlash('Data telah disimpan.');
	      $this->redirect(array('action' => 'index'));
	    }
	  }
	 
	  $this->request->data = $this->Artikel->read();
	  $this->render('tambah');
	}

	function hapus($id)
	{
	  if ($this->Artikel->delete($id))
	  {
	    $this->Session->setFlash('Data telah dihapus.');
	    $this->redirect(array('action' => 'index'));
	  }
	}

	function lihat($id)
	  {
	  $this->Artikel->id = $id;
	  $this->set('artikel',$this->Artikel->read());
	  }
}